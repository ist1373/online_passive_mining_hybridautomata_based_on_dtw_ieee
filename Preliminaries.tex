
\section{Preliminaries}
\label{sec:pre}

In this section, we briefly present the preliminary concepts of the paper. The definitions related to the problem input, its output, and the underlying technique of our proposed method are presented in Sections~\ref{sec:trace},~\ref{sec:automata}, and~\ref{sec:warp}, respectively.

\subsection{Trace Modeling}
\label{sec:trace}

As mentioned, our input is a set of input/output traces of the system. Here, we give a brief definition of these traces.

\begin{definition}{Trace of Signal:}
A trace of signal   with the sampling period $c$  is a finite sequence of pairs of timestamps and values $ (t_1,v_1)\cdots(t_p,v_p) $, such that:


\begin{itemize}
	\item $\forall i \in [1,p]:t_i \in \mathbb{R}^{\geq 0}$
	\item $\forall i \in [2,p]: t_i-t_{i-1} = c $ 
	\item $ \forall i \in [1,p]: v_i $ is the value of the signal $S$ at time $t_i$ 
\end{itemize}

\end{definition}

\noindent  As mentioned, our input is a set of $n$ input signals $\{I^1,\cdots,I^n\}$, and $m$ output signals $\{O^1,\cdots,O^m\}$. An input/output trace, represented by $w$ consists of input and output signals.

\begin{definition}{ Input/Output Trace:}
	\label{def:io}
	 An input/output trace is represented by a sequence of tuples:
	\begin{equation}
	\nonumber
	\begin{split}
	 w =  &(t_1,I^1_1,\cdots,I^n_1,O^1_1,\cdots,O^m_1),\cdots,\\
	&(t_p,I^1_p,\cdots,I^n_p,O^1_p,\cdots,O^m_p)\\
	\end{split}
	\end{equation} 
\end{definition}

\noindent , where each tuple consists of a timestamp, and the values of input and output signals at that time.

\iffalse
\textbf{Definition 2} [Sampled Trace]: 
\\

\noindent Each Sample S consists of $ n $ input sampled traces $ w^i_{1},w^i_{2},...,w^i_{n} $ and $ m $ output sampled traces $ w^o_{1},w^o_{2},...,w^o_{m} $ such that:\\
$- \forall k \in [1,n] , \forall j \in [1,m]: t_{p_k} = t_{p_j} $(all sampled traces of S have an equal length)\\


\textbf{Definition 3} [Input/Output Trace]: An \textit{Input/Output Trace $ w^{IO}_s $} of signal $S$ is a finite sequence of tuples such that:
\begin{equation}
	\begin{split}
		& w^{IO}_s =  (t_1,v_1^{w^i_{1}},...,v_1^{w^i_{n}},v_1^{w^o_{1}},...,v_1^{w^o_{m}})...\\
		&(t_p,v_p^{w^i_{1}},...,v_p^{w^i_{n}},v_p^{w^o_{1}},...,v_p^{w^o_{m}})\\
	\end{split}
\end{equation} 
\fi


\begin{definition}{Segmented Input/Output Trace:}
		\label{def:segio}
	A segmented input/output trace $ \psi^{w}_{k,j} $ of a trace $w$ ($ k,j \in [1,p]  ,  k < j $) is a subsequence of $w$:
	\begin{equation}
	\nonumber
	\begin{split}
	 \psi^{w}_{k,j} = 
	 &(t_k,I^1_k,\cdots,I^n_k,O^1_k,\cdots,O^m_k),\cdots,\\
	 &(t_j,I^1_j,\cdots,I^n_j,O^1_j,\cdots,O^m_j)\\
	\end{split}
	\end{equation} 

	
\end{definition}

\iffalse
\textbf{Definition 4} [Segmented Input/Output Trace]: a \textit{Segmented Input/Output Trace $ \psi^{s}_{k,j} $} of signal S is a finite sequence of tuples such that:
\begin{equation}
	\begin{split}
		& \psi^{s}_{k,j} =  (t_k,v_k^{w^i_{1}},..,v_k^{w^i_{n}},v_k^{w^o_{1}},..,v_k^{w^o_{m}})...\\
		&(t_j,v_j^{w^i_{1}},..,v_j^{w^i_{n}},v_j^{w^o_{1}},..,v_j^{w^o_{m}})\\
	\end{split}
\end{equation} 
such that:\\
$ k,j \in [1,p]  ,  k < j $\\
\fi

\subsection{Hybrid Automata}
\label{sec:automata}
The final output of our algorithm is a hybrid automaton. To give an intuition,
consider the automaton of a thermostat system  depicted in Fig.~\ref{therm_hyb}. 
Similar to every state-transition model, a hybrid automaton consists of a set of states and a set of transitions. As an example, the automaton in Fig.~\ref{therm_hyb} has two states, \textit{on} and \textit{off}, and two transitions between them. In a hybrid automaton, there is also a set of {\em continuous variables}. For instance, in the example automaton, the temperature of the environment is a continuous variable  denoted by $x$. A hybrid automaton specifies the valuation of the continuous variables at each state by a predicate. For example, in Fig.~\ref{therm_hyb}, the change rate of $ x $ at state {\em off} is denoted by $ \dot{x} = -0.1x $, where $\dot{x}$ is the first derivation of the variable $ x $ with respect to time. Similarly, the change rate of the variable $ x $ at state {\em on} is specified as $ \dot{x} = 5 - 0.1x $.

\input{Thermostat_Hybrid}

\begin{definition}
 A \textit{hybrid automaton} $H$  is a tuple $\langle X, G , \flow , \jump 
\rangle$, where~\cite{henzinger2000theory}:

\begin{itemize}
	\item $X=\{x_1,...,x_n\}$ is a finite set of real-numbered \textit{variables}.  The number $n$ is called the dimension of $H$.  $\dot{X}= \{\dot{x}_1,...,\dot{x}_n\} $, called the set of dotted variables, represents the first derivatives of variables used for showing their continuous changes. $ X'=\{x'_1,...,x'_n\} $, called the primed variables, represents the values of variables  after the discrete changes.
	\item $G= (V,E)$ is a finite directed multigraph, where the vertices $V$ are called the \textit{control modes}, and  the edges $E$ are called the \textit{control switches}. In Fig.~\ref{therm_hyb}, there are two control modes $ V \in \{\textnormal{on,off}\} $ and two switches $ E \in \{\textnormal{on}\rightarrow \textnormal{off}, \textnormal{off} \rightarrow \textnormal{on}\} $.
%	\item $\init$ is a labeling function $V \rightarrow \pred$ that assigns to each control mode $ v \in V $ an initial condition $ init(v) $ from the set of predicates on the variables $X$. In Fig.~\ref{therm_hyb}, the initial value of $ x $ in mode off is 20. so $ init(\textnormal{off}) = \{x'=20\} $

%	\item $\inv$ is a labeling function $V \rightarrow \pred$ that assigns to each control mode $ v \in V $ a condition $ inv(v) $ that holds, while the system is at that mode. In Fig.~\ref{therm_hyb}, the invariant of mode off is $ x \ge 19 $.
	\item $\flow$ is a labeling function $V \rightarrow \pred$ that assigns to each control mode $ v \in V $ a flow condition $ flow(v) $. Each flow condition $flow(v)$ is a predicate whose free variables are from $ X \cup \dot{X} $. In Fig.~\ref{therm_hyb}, the continuous change of $ x $ at mode on is represented by $ \{\dot{x}=5-0.1x\} $
	\item $\jump$ is an edge labeling function $ E \rightarrow \pred$ that assigns to each control switch $ e \in E $ a predicate $\jump(e)$. Each jump condition $ jump(e) $ is a predicate whose free variables are from $ X \cup X'$. Each label can be a jump condition or  a variable assignment. In Fig.~\ref{therm_hyb}, predicate $ x > 21 $ is a jump condition on the control switch $\textnormal{on} \rightarrow \textnormal{off}$ and $ x'=20 $ is an assignment to the variable $x$. Note that control modes that are the destination of  control switches without a source control mode are the initial control modes, and the assignments of these control switches represent the initial values of the variables.
	
%	\item \textit{Events.} A finite set $ \Sigma $ of events, and an edge labeling function  $ event: E \rightarrow \Sigma $ that assign to each control switch an event. In Fig.~\ref{therm_hyb}, event \textit{turn on} has been assigned to control switch $\textnormal{off} \rightarrow \textnormal{on}$.
\end{itemize}

\end{definition}






\subsection{Dynamic Time Warping Analysis}
\label{sec:warp}

Our synthesis algorithm is based on 
Dynamic Time Warping (DTW).  It was first introduced in~\cite{bellman1959adaptive} to detect similarity between two nonlinear time series. The technique has been widely used in several areas, such as speech, handwriting and gesture recognition, signal processing, data mining, and time series clustering. DTW employs dynamic programming to calculate the optimal match between two given time series $ X=(x_1,...,x_N) $ and $ Y = (y_1,...,y_M) $ with the both time and storage complexity of $ O(N M) $~\cite{senin2008dynamic}.  As an example, consider two time series $X$ and $Y$ that are depicted in the right plot of Fig.~\ref{fig3}. DTW tries to find the most similar corresponding data points between the two time series. The left plot in Fig.~\ref{fig3} demonstrates the alignment matrix of time series $X$ and $Y$. This matrix indicates the corresponding data points in time series $X$ and $Y$. The diagonality in the emerging path shows the similarity between the two time series. 

Given two sequences $ X $ and $ Y $, the algorithm constructs a distance matrix $C$ for their alignment:

\begin{equation}
	C \in \mathbb{R}^{N\times M}:c_{ij}=||x_i-y_j||, i \in [1:N], j \in [1:M]
\end{equation}

\noindent DTW builds an alignment path $ P = (p_1,p_2,...,p_k)$, such that $ \forall l \in [1,k]: p_l=(i,j) : i \in [1:N] \;,\; j \in [1:M]$,
and finally it finds a path with minimum distance from $ p_1=(1,1) $ to $ p_k = (N,M) $. The optimal distance of time series $ X $ and $ Y $ is calculated by Eq.~\ref{eq3}.

\begin{equation}
	\label{eq3}
	dist^{DTW}(X,Y) = \sum_{l=1}^{k}c_{i_{p_l}j_{p_l}}
\end{equation} 


DTW analysis not only calculates the minimum distance between two time series, but it also determines the corresponding data points in the two time series.
We denote the first elements of path $ P $ as $P_i= (i_{p_1},i_{p_2},...,i_{p_k}) $ and the second elements of path $ P $ as $ P_j= (j_{p_1},j_{p_2},...,j_{p_k}) $. Then we could measure the diagonality of path $ P $ as follows:

\begin{equation}
	\label{eq4}
	diag^{DTW}(X,Y) = correlation(P_i,P_j)
\end{equation} 




%\begin{figure}[htbp]
%	\centerline{\includegraphics[width=00.8\columnwidth]{dtw.png}}
%	\caption{Dynamic Time Warping cost matrix could determine the corresponding points between reference and query signals}
%	\label{fig2}
%\end{figure}

  












